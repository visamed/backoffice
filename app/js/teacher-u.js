var myApp = angular.module("myApp",[]);

myApp.controller("header",['$scope',function($scope){
	$scope.adminName = " Admin";
	$scope.schoolWebSite = "http://www.myScholl.com";
	$scope.frontOfficeWebSite = "http://www.frontOfficeWebSite.com";
}]);

myApp.controller("menu",['$scope',function($scope){
	$scope.menuSectionShow = [true,true,true,true,true];
	$scope.userShow = [true,true];
	$scope.userRolesShow = [true,true,true,true,true];
	$scope.userDetailsShow = [true,true,true];
	$scope.classesShow = [true,true,true];
	$scope.activitiesShow = [true,true,true,true,true,true,true];
	$scope.configurationShow = [true,true,true];
}]);