var myApp = angular.module("myApp",[]);

myApp.controller("header",['$scope',function($scope){
	$scope.adminName = " Admin";
	$scope.schoolWebSite = "http://www.myScholl.com";
	$scope.frontOfficeWebSite = "http://www.frontOfficeWebSite.com";
}]);

myApp.controller("menu",['$scope',function($scope){
	$scope.menuSectionShow = [true,true,true,true,true];
	$scope.userShow = [true,true];
	$scope.userRolesShow = [true,true,true,true,true];
	$scope.userDetailsShow = [true,true,true];
	$scope.classesShow = [true,true,true];
	$scope.activitiesShow = [true,true,true,true,true,true,true];
	$scope.configurationShow = [true,true,true];
}]);

myApp.controller("users",['$scope',function($scope){
	$scope.supervisorsNumber = [4,1];
	$scope.staffNumber = [227,5];
	$scope.studentsNumber = [2249,32];
	$scope.totalMembers = [];
	$scope.totalMembers[0] = $scope.supervisorsNumber[0]+$scope.staffNumber[0]+$scope.studentsNumber[0];
	$scope.totalMembers[1] = $scope.supervisorsNumber[1]+$scope.staffNumber[1]+$scope.studentsNumber[1];
	$scope.subjecstNumber = [32,3];
	$scope.levelsNumber = [4,0];
	$scope.classesNumber = [34,2];
	$scope.messageNumber = [24,13];
}]);

myApp.controller("memberActiviy",['$scope',function($scope){
	$scope.activityDetail = [{	name:'Usman',
								image:'http://www.gravatar.com/avatar/3232415a0380253cfffe19163d04acab.png?s=50',
								date:'20/05/2014',
								activity:'Chat',
								link:'#'},

							{	name:'Sheikh Heera',
								image:'http://www.gravatar.com/avatar/3232415a0380253cfffe19163d04acab.png?s=50',
								date:'19/05/2014',
								activity:'Exam',
								link:'#'},

							{	name:'Abdullah',
								image:'http://www.gravatar.com/avatar/3232415a0380253cfffe19163d04acab.png?s=50',
								date:'18/05/2014',
								activity:'Calendar',
								link:'#'},

							{	name:'Sana Amrin',
								image:'http://www.gravatar.com/avatar/3232415a0380253cfffe19163d04acab.png?s=50',
								date:'17/05/2014',
								activity:'Notice',
								link:'#'}];
}]);

myApp.controller("notice",['$scope',function($scope){
	$scope.noticeDetail = [{	title:'title',
								date:'20/06/2014',
								status:'Created',
								color:'label-success label label-default'},

							{	title:'title',
								date:'14/06/2014',
								status:'Deleted',
								color:'label-default label label-danger'},

							{	title:'title',
								date:'21/05/2014',
								status:'Updated',
								color:'label label-info'},

							{	title:'title',
								date:'13/05/2014',
								status:'Created',
								color:'label-success label label-default'}];
}]);

myApp.controller("schoolCalendar",['$scope',function($scope){
	$scope.calendarDetail =	[{	applicant:'Usman',
								date:'25/02/2015',
								status:'Approved',
								color:'label-success label label-default'},

							{	applicant:'Sheikh Heera',
								date:'02/02/2014',
								status:'Pending',
								color:'label-warning label label-default'},

							{	status:'Abdullah',
								date:'13/01/2014',
								status:'Banned',
								color:'label label-info'},

							{	status:'Sana Amrin',
								date:'11/01/2014',
								status:'Updated',
								color:'label-default label label-danger'}];
}]);